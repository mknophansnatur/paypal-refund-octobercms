# Basic PHP container for PHP editing with Emacs.
#
# Twitter: @maridonkers | Google+: +MariDonkers | GitHub: maridonkers.
#

FROM ubuntu

ENV DEBIAN_FRONTEND=noninteractive

RUN sed -i "s#\smain\s*\$# main contrib non-free#" /etc/apt/sources.list

RUN apt-get update && apt-get install -yq procps sudo git-core zip curl gnupg
RUN apt-get install -yq emacs25 vim silversearcher-ag

# Repository for newer PHP versions (Debian Stretch has 7.0 but e.g. phpactor requires >=7.1).
# https://tecadmin.net/install-php-debian-9-stretch/
RUN apt-get install -yq ca-certificates apt-transport-https
RUN curl -sS https://packages.sury.org/php/apt.gpg | apt-key add -
RUN echo "deb https://packages.sury.org/php/ stretch main" >> /etc/apt/sources.list.d/php.list
RUN apt-get install -y software-properties-common
RUN apt-add-repository -y ppa:ondrej/php
RUN sudo apt-get -y update
RUN apt-get -y install  \
    php7.2 \
    php7.2-fpm \
    php7.2-dev \
    php7.2-cli \
    php7.2-common \
    php7.2-intl \
    php7.2-bcmath \
    php7.2-mbstring \
    php7.2-soap \
    php7.2-xml \
    php7.2-zip \
    php7.2-json \
    php7.2-gd \
    php7.2-curl \
    php7.2-mysql \
    mysql-client

ENV COMPOSER_ALLOW_SUPERUSER=1
RUN mkdir -p /opt/php
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/opt/php --filename=composer
RUN ln -s /opt/php/composer /usr/local/bin/composer

WORKDIR /opt/php
RUN curl -LO https://github.com/phpstan/phpstan/releases/download/0.10.5/phpstan.phar
RUN chmod +x phpstan.phar
RUN ln -s /opt/php/phpstan.phar /usr/local/bin/phpstan

WORKDIR /opt/php
RUN curl -LO https://psysh.org/psysh
RUN chmod +x psysh
RUN ln -s /opt/php/psysh /usr/local/bin/psysh

RUN sed -i "s#^\(www-data:.*:\)/usr/sbin/nologin#\1/bin/bash#" /etc/passwd
RUN echo "www-data ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
WORKDIR /var/www

ENV DISPLAY=:0
ENTRYPOINT ["/bin/bash"]
