FROM php:7.2.2-fpm

RUN apt-get update && \
    apt-get install -y cron-apt mysql-client \
    && rm -rf /var/lib/apt \
    && docker-php-ext-install pdo_mysql
 

COPY . /var/www


# Uncomment this, if you need 2 do stuff before running the serivce. 
# ENTRYPOINT php artisan key:generate && php artisan migrate && php-fpm  
