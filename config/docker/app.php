<?php
/*
| For additional options see
| https://github.com/octobercms/october/blob/master/config/app.php
*/

return [
    'debug' =>  true,
    'url' => 'http://localhost',
    'timezone' => 'TZ', 'UTC',
    'key' => '0123456789ABCDEFGHIJKLMNOPQRSTUV'
];
