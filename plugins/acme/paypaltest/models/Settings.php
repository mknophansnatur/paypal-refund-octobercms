<?php

namespace Acme\PaypalTest\Models;

use Model;

class Settings extends Model
{
	public $implement = ['System.Behaviors.SettingsModel'];

	// A unique code
	public $settingsCode = 'acme_paypaltest_settings';

	// Reference to field configuration
	public $settingsFields = 'fields.yaml';

	public function __construct()
	{
		// Set a single value
		Settings::set('api_key', 'ABCD');

		// Set an array of values
		Settings::set(['api_key' => 'ABCD']);

		parent::__construct();

		BackendMenu::setContext('October.System', 'system', 'settings');
		SettingsManager::setContext('Paypaltest.Plugin', 'settings');

		$settings = Settings::instance();
		$settings->api_key = 'ABCD';
		$settings->save();
	}
}

