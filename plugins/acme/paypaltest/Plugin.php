<?php
namespace Acme\PaypalTest;

class Plugin extends \System\Classes\PluginBase
{
	public function pluginDetails()
	{
		return [
			'name' => 'Paypal Plugin',
			'description' => 'Paypal Plugin to show the Refund Stuff',
			'author' => 'ACME Corporation',
			'icon' => 'icon-leaf'
		];
	}

	public function registerComponents()
	{
		return [
			'Acme\PaypalTest\Components\Paypal' => 'Paypal'
		];
	}

	public function registerSettings()
	{
		return [
			'settings' => [
				'label'       => 'User Settings',
				'description' => 'Manage user based settings.',
				'category'    => 'Users',
				'icon'        => 'icon-cog',
				'class'       => 'Acme\User\Models\Settings',
				'order'       => 500,
				'keywords'    => 'security location',
				'permissions' => ['acme.users.access_settings']
			]
		];
	}
}
