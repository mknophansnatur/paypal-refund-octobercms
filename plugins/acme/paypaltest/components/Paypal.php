<?php
namespace Acme\PaypalTest\Components;

use Acme\PaypalTest\Models\Settings;

class Paypal extends \Cms\Classes\ComponentBase
{


	public function componentDetails()
	{
		return [
			'name' => 'Paypal Komponente',
			'description' => 'Paypal Komponente zum anzeigen des Paypal Refund Tools'
		];
	}


// This array becomes available on the page as {{ component.posts }}
	public function functioneins()
	{
		// Outputs: ABCD
		echo Settings::instance()->api_key;

		// Get a single value
		echo Settings::get('api_key');

		echo Settings::get('is_activated', true);


		echo "hello world";
	}



}

