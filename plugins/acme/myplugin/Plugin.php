<?php
namespace Acme\MyPlugin;

class Plugin extends \System\Classes\PluginBase
{
	public function pluginDetails()
	{
		return [
			'name' => 'Test Plugin',
			'description' => 'Provides some really cool stuff.',
			'author' => 'ACME Corporation',
			'icon' => 'icon-leaf'
		];
	}

	public function registerComponents()
	{
		return [
			'Acme\MyPlugin\Components\ComponentName' => 'ComponentName'
		];
	}
}
