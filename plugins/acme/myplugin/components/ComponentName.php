<?php
namespace Acme\MyPlugin\Components;

class ComponentName extends \Cms\Classes\ComponentBase
{
	public function componentDetails()
	{
		return [
			'name' => 'Test Dingens',
			'description' => 'First component'
		];
	}

	public function defineProperties()
	{
		return [
			'maxItems' => [
				'title'             => 'Max items',
				'description'       => 'The most amount of todo items allowed',
				'default'           => 10,
				'type'              => 'string',
				'validationPattern' => '^[0-9]+$',
				'validationMessage' => 'The Max Items property can contain only numeric symbols'
			]
		];
	}

	// This array becomes available on the page as {{ component.posts }}
	public function posts()
		{
			return ['First Post', 'Second Post', 'Third Post'];
		}
	}

