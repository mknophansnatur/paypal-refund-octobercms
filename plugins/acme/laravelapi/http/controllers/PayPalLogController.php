<?php

namespace Acme\LaravelApi\Http\Controllers;
use Acme\LaravelApi\PayPalLog;

use Illuminate\Http\Request;

use Illuminate\Routing\Controller;

class PayPalLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($transactionId)
    {
//        return json_encode(PayPalLog::where("transaction_id", $transactionId)->get()->toArray());
		return json_encode(PayPalLog::where("transaction_id", $transactionId)->get()->toArray());
//    	return $transactionId;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PayPalLog  $payPalLog
     * @return \Illuminate\Http\Response
     */
    public function show($transactionId, $refundId)
    {
        return json_encode(PayPalLog::where("transaction_id", $transactionId)
                ->where("refund_id", $refundId)
                ->get());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PayPalLog  $payPalLog
     * @return \Illuminate\Http\Response
     */
    public function edit(PayPalLog $payPalLog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PayPalLog  $payPalLog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PayPalLog $payPalLog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PayPalLog  $payPalLog
     * @return \Illuminate\Http\Response
     */
    public function destroy(PayPalLog $payPalLog)
    {
        //
    }
}
