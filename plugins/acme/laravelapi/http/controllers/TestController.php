<?php namespace Acme\LaravelApi\Http\Controllers;

use Acme\LaravelApi\Token;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class TestController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function index($test)
	{
		return view('acme.laravelapi::test');
	}

	public function show($transactionId) {
    	//Token::login();

//		Schema::create('configs', function (Blueprint $table) {
//			$table->bigIncrements('id');
//			$table->string('clientid');
//			$table->string('clientsecret');
//			$table->timestamps();
//		});


		$curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL            => "https://api.sandbox.paypal.com/v1/payments/sale/" . $transactionId,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_POSTFIELDS     => "",
            CURLOPT_HTTPHEADER     => array(
                "Authorization: Bearer " . Token::getToken(),
                "Content-Type: application/json",
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err      = curl_error($curl);
        return $response;
	}
}
