<?php

namespace App\Http\Controllers;

use App\Credentials;
use App\PayPalRefund;
use App\Token;

class DebugController extends Controller
{
    private $token;
    private $credentials;
    private $paypalRefund;

    public function __construct(Token $token, Credentials $credentials, PayPalRefund $paypalRefund)
    {
        $this->token        = $token;
        $this->credentials  = $credentials;
        $this->paypalRefund = $paypalRefund;
    }

    public function index()
    {
        $this->token->login("AXW1AWacXjdCWzzZAER-b7olK2lXqSNwcH7k35o2iqHbuUOPxTYYtSOA7NmUvaMCWHzhlXsuHzjhi6iS", "EO9azBfjve2NmAxCmtRfycZkZy9MaTQDDCiq-wiNzHWrykG4qy6xkKYl5QisBtxrR6nFq1pyCI-gg2-a");
        $this->paypalRefund->refund("1X093783AR432182A", "5.22");
    }
}
