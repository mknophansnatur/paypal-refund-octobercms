<?php

namespace Acme\LaravelApi\Http\Controllers;

use Acme\LaravelApi\PayPalRefund;

use Illuminate\Http\Request;


class PayPalRefundController extends Controller
{
    private $paypalRefund;

//    public function __construct(PayPalRefund $paypalRefund)
//    {
//        $this->paypalRefund = $paypalRefund;
//
//    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

/**
 * Show the form for creating a new resource.
 *
 * @return \Illuminate\Http\Response
 */
    public function create()
    {

    }

/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
//    public function store($id, Request $request)
//    {
//        //return $this->paypalRefund->refund($id, $request->all());
//    }

	/**
	 * Display the specified resource.
	 *
	 * @param $transactionId
	 * @return \Illuminate\Http\Response
	 */
    public function show($transactionId)
    {

//        $curl = curl_init();
//        curl_setopt_array($curl, array(
//            CURLOPT_URL            => "https://api.sandbox.paypal.com/v1/payments/sale/" . $transactionId,
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING       => "",
//            CURLOPT_MAXREDIRS      => 10,
//            CURLOPT_TIMEOUT        => 30,
//            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST  => "GET",
//            CURLOPT_POSTFIELDS     => "",
//            CURLOPT_HTTPHEADER     => array(
//                "Authorization: Bearer " . Token::getToken(),
//                "Content-Type: application/json",
//                "cache-control: no-cache",
//            ),
//        ));
//
//        $response = curl_exec($curl);
//        $err      = curl_error($curl);
//        return $response;
		return $transactionId;
    }
//
///**
// * Show the form for editing the specified resource.
// *
// * @param  \App\PayPalRefund  $payPalRefund
// * @return \Illuminate\Http\Response
// */
//    public function edit(PayPalRefund $payPalRefund)
//    {
//        //
//    }
//
///**
// * Update the specified resource in storage.
// *
// * @param  \Illuminate\Http\Request  $request
// * @param  \App\PayPalRefund  $payPalRefund
// * @return \Illuminate\Http\Response
// */
//    public function update(Request $request, PayPalRefund $payPalRefund)
//    {
//        //
//    }
//
///**
// * Remove the specified resource from storage.
// *
// * @param  \App\PayPalRefund  $payPalRefund
// * @return \Illuminate\Http\Response
// */
//    public function destroy(PayPalRefund $payPalRefund)
//    {
//        //
//    }
}
