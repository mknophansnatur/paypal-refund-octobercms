<?php

namespace Acme\LaravelApi;

//use Token;
use Illuminate\Database\Eloquent\Model;

class PayPalRefund extends Model
{
    protected $fillable = [
    ];

    public function refund($transactionId, $partial = null)
    {

//        $url = "https://api.paypal.com/v1/payments/sale/";
		$url = "https://api.sandbox.paypal.com/v1/payments/sale/";
//		if (env("SANDBOX")) {
//        }

        //return error if body is empty
        if ($partial == null) {
            return json_encode(["Error" => "JSON BODY muss gesetzt sein!"]);
        }
        // check if some of the elements are empty or not set
        if (empty($partial["RefundDescription"]) || empty($partial["RefundValue"]) || empty($partial["RefundReference"])) {
            return json_encode(["Error" => "JSON BODY muss gesetzt sein!"]);
        }
        $payload = json_encode([
            "amount"         => [
                "total"    => $partial["RefundValue"],
                "currency" => "EUR",
            ],
            "invoice_number" => $partial["RefundReference"],
            "description"    => $partial["RefundDescription"],
        ]);

        //$bToken = Token::getToken();
		$bToken = '';
        $curl   = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL            => $url . $transactionId . "/refund",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_POSTFIELDS     => $payload,
            CURLOPT_HTTPHEADER     => array(
                "Authorization: Bearer " . $bToken,
                "Content-Type: application/json",
                "cache-control: no-cache",
            ),
        ));
        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        }

        $decodedResponse = json_decode($response, true);
        $success         = false;
        if (isset($decodedResponse["sale_id"])) {
            $success = true;
        }
        $logEntry = PayPalLog::create([
            "transaction_id"     => $transactionId,
            "refund_id"          => $decodedResponse["id"] ?? null,
            "refund_value"       => $decodedResponse["amount"]["total"] ?? null,
            "refund_reference"   => $decodedResponse["invoice_number"] ?? null,
            "refund_description" => $decodedResponse["description"] ?? null,
            "successfull"        => $success,
            "refund_entity"      => json_encode($decodedResponse),
        ]);
        return $logEntry;
    }
}
