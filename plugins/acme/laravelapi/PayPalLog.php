<?php

namespace Acme\LaravelApi;

use Illuminate\Database\Eloquent\Model;

class PayPalLog extends Model
{
    protected $fillable = [
        "transaction_id",
        "refund_id",
        "refund_value",
        "refund_reference",
        "refund_description",
        "successfull",
        "refund_entity",
    ];
}
