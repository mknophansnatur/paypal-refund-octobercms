<?php

Route::get('/welcome', 'Acme\LaravelApi\Http\Controllers\WelcomeController@index')->middleware('web');

// POST    /{TransactionId}/refund    trigger refund and return it
Route::get('/lul/{test}', 'Acme\LaravelApi\Http\Controllers\TestController@show');

// POST    /{TransactionId}/refund    trigger refund and return it
Route::post('/refund', 'Acme\LaravelApi\Http\Controllers\PayPalRefundController@index');

// GET    /{TransactionId}    get transaction from paypal api
Route::get('/paypal/{transactionid}', 'Acme\LaravelApi\Http\Controllers\PayPalRefundController@show');
