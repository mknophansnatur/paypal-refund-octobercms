<?php

namespace Acme\LaravelApi;

use Illuminate\Database\Eloquent\Model;
use Acme\LaravelApi\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Token extends Model
{

    protected $fillable = [

        "token",
        "expire",
    ];

    public static function login($clientId = "AS21W51EXBVPIB3vVJhoD6aUVesBarFDNsDvaLbFeR_MJJvxwsvGyVnVFZXzXxArSyREBjcuO7gW-cNr", $clientSecret = "EB3AJgtmHNke0UZ_V-bFfYIJK_jgUnxs2ERYmILPCJjoNb0kDfLdm9iGZjiJxeV1Af912lFBrMJd1Xxy")
    {
//        $url = "https://api.paypal.com/v1/oauth2/token";
		$url = "https://api.sandbox.paypal.com/v1/oauth2/token";
//        if (env("SANDBOX")) {
//            $url = "https://api.sandbox.paypal.com/v1/oauth2/token";
//        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_POSTFIELDS     => "grant_type=client_credentials",
            CURLOPT_HTTPHEADER     => array(
                "authorization: Basic " . base64_encode($clientId . ":" . $clientSecret),
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
            ),
        ));

        $response = curl_exec($curl);
        $err      = curl_error($curl);
        curl_close($curl);
        $decodedResponse = json_decode($response, true);

		Schema::create('tokens', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('token');
			$table->dateTime('expire');
			$table->timestamps();
		});


		Token::truncate();
        $currentToken = Token::create([
            'token'  => $decodedResponse["access_token"],
            "expire" => date("Y-m-d H:i:s", strtotime("+" . ($decodedResponse["expires_in"] - 3600) . "seconds")),
        ]);

        return $currentToken;
    }

    public static function getToken()
    {
        return self::checkTokenTimestamp(Token::first());
    }
    public static function checkTokenTimestamp(Token $token = null)
    {

		$config = Config::first();


        //if no token is set or empty db check
        if (!isset($token)) {
            self::login();
            return self::getToken();
        }

        //check if the expire date is valid
        if (date("Y-m-d H:i:s") <= $token->expire) {
            return $token->token;
        }

        self::login($config->clientid, $config->clientsecret);
        return self::getToken();
    }
}
