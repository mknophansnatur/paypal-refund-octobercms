<?php namespace MelvinKnop\Paypaltoken\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMelvinknopPaypaltokenToken extends Migration
{
    public function up()
    {
        Schema::create('melvinknop_paypaltoken_token', function($table)
        {
            $table->engine = 'InnoDB';
            $table->string('token');
            $table->date('expire');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('melvinknop_paypaltoken_token');
    }
}
