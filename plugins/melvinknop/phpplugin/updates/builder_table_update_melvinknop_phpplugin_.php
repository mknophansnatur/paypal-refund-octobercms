<?php namespace MelvinKnop\PhpPlugin\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMelvinknopPhpplugin extends Migration
{
    public function up()
    {
        Schema::table('melvinknop_phpplugin_', function($table)
        {
            $table->string('clientid')->default('AS21W51EXBVPIB3vVJhoD6aUVesBarFDNsDvaLbFeR_MJJvxwsvGyVnVFZXzXxArSyREBjcuO7gW-cNr');
            $table->string('clientsecret')->default('EB3AJgtmHNke0UZ_V-bFfYIJK_jgUnxs2ERYmILPCJjoNb0kDfLdm9iGZjiJxeV1Af912lFBrMJd1Xxy');
        });
    }
    
    public function down()
    {
        Schema::table('melvinknop_phpplugin_', function($table)
        {
            $table->dropColumn('clientid');
            $table->dropColumn('clientsecret');
        });
    }
}
