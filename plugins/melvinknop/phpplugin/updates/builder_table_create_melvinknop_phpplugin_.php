<?php namespace MelvinKnop\PhpPlugin\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMelvinknopPhpplugin extends Migration
{
    public function up()
    {
        Schema::create('melvinknop_phpplugin_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('user', 32);
            $table->string('password');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('melvinknop_phpplugin_');
    }
}
