<?php namespace MelvinKnop\PhpPlugin\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class PaypalController extends Controller
{
    public $implement = [        'Backend\Behaviors\FormController'    ];
    
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('MelvinKnop.PhpPlugin', 'PaypalPlugin');
    }
}
