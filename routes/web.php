<?php

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', function () {
    return view("welcome");});
Route::get("debug/gettoken", "DebugController@index");

//GET    /{TransactionId}/refund/{RefundId}    get refund entity
Route::get('/{transactionid}/refund/{refundid}', 'PayPalLogController@show');

//GET    /{TransactionId}/refund     get all refund entities
Route::get('/{transactionid}/refund', 'PayPalLogController@index');

// POST    /{TransactionId}/refund    trigger refund and return it
Route::post('/{transactionid}/refund', 'PayPalRefundController@store');

// GET    /{TransactionId}    get transaction from paypal api
Route::get('paypal/{transactionid}', 'PayPalRefundController@show');

Route::get('/config', 'ConfigController@index')->name("config-index");
Route::post('/config', 'ConfigController@store')->name("config-store");
